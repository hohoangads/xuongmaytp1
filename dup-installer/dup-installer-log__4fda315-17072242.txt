********************************************************************************
* DUPLICATOR-PRO: Install-Log
* STEP-1 START @ 08:06:02
* VERSION: 1.3.36
* NOTICE: Do NOT post to public sites or forums!!
********************************************************************************
PACKAGE INFO________ CURRENT SERVER                         |ORIGINAL SERVER
PHP VERSION_________: 7.1.9                                 |7.3.26
OS__________________: Linux                                 |Linux
CREATED_____________: 2020-08-17 07:22:42
WP VERSION__________: 4.9.15
DUP VERSION_________: 1.3.36
DB__________________: 10.0.32
DB TABLES___________: 35
DB ROWS_____________: 2,054
DB FILE SIZE________: 5.53MB
********************************************************************************
SERVER INFO
PHP_________________: 7.3.26 | SAPI: fpm-fcgi
PHP MEMORY__________: 4294967296 | SUHOSIN: disabled
SERVER______________: nginx/1.18.0
DOC ROOT____________: "/home/store.ztech.vn/public_html"
DOC ROOT 755________: true
LOG FILE 644________: true
REQUEST URL_________: "http://store.ztech.vn/dup-installer/main.installer.php"
********************************************************************************
USER INPUTS
ARCHIVE ENGINE______: "ziparchive"
SET DIR PERMS_______: 1
DIR PERMS VALUE_____: 1363
SET FILE PERMS______: 1
FILE PERMS VALUE____: 1204
SAFE MODE___________: "0"
LOGGING_____________: "1"
CONFIG MODE_________: "NEW"
FILE TIME___________: "current"
********************************************************************************


--------------------------------------
ARCHIVE SETUP
--------------------------------------
NAME________________: "/home/store.ztech.vn/public_html/20200817_trangwebmoi_[HASH]_20200817072242_archive.zip"
SIZE________________: 60.96MB

PRE-EXTRACT-CHECKS
- PASS: Apache '.htaccess' not found - no backup needed.
- PASS: Microsoft IIS 'web.config' not found - no backup needed.
- PASS: WordFence '.user.ini' not found - no backup needed.


START ZIP FILE EXTRACTION STANDARD >>> 

--------------------------------------
ARCHIVE SETUP
--------------------------------------
NAME________________: "/home/store.ztech.vn/public_html/20200817_trangwebmoi_[HASH]_20200817072242_archive.zip"
SIZE________________: 60.96MBFile timestamp set to Current: 2021-01-28 08:06:03
<<< ZipArchive Unzip Complete: true
--------------------------------------
POST-EXTACT-CHECKS
--------------------------------------
PERMISSION UPDATES:
    -DIRS:  '755'
    -FILES: '644'
[PHP ERR][WARN] MSG:touch(): Utime failed: Permission denied [CODE:2|FILE:/home/store.ztech.vn/public_html/dup-installer/ctrls/ctrl.s1.php|LINE:407]
[PHP ERR][WARN] MSG:touch(): Utime failed: Permission denied [CODE:2|FILE:/home/store.ztech.vn/public_html/dup-installer/ctrls/ctrl.s1.php|LINE:407]

STEP-1 COMPLETE @ 08:06:03 - RUNTIME: 1.8232 sec.
[PHP ERR][WARN] MSG:mysqli_connect(): (HY000/1045): Access denied for user 'user_store'@'localhost' (using password: YES) [CODE:2|FILE:/home/store.ztech.vn/public_html/dup-installer/classes/class.db.php|LINE:48]
DATABASE CONNECTION ERROR: Access denied for user 'user_store'@'localhost' (using password: YES)[ERRNO:1045]
[PHP ERR][WARN] MSG:mysqli_connect(): (HY000/1045): Access denied for user 'user_store'@'localhost' (using password: YES) [CODE:2|FILE:/home/store.ztech.vn/public_html/dup-installer/classes/class.db.php|LINE:48]
DATABASE CONNECTION ERROR: Access denied for user 'user_store'@'localhost' (using password: YES)[ERRNO:1045]



********************************************************************************
* DUPLICATOR-LITE INSTALL-LOG
* STEP-2 START @ 08:07:14
* NOTICE: Do NOT post to public sites or forums!!
********************************************************************************
USER INPUTS
VIEW MODE___________: "basic"
DB ACTION___________: "empty"
DB HOST_____________: "**OBSCURED**"
DB NAME_____________: "**OBSCURED**"
DB PASS_____________: "**OBSCURED**"
DB PORT_____________: "**OBSCURED**"
NON-BREAKING SPACES_: false
MYSQL MODE__________: "DEFAULT"
MYSQL MODE OPTS_____: ""
CHARSET_____________: "utf8"
COLLATE_____________: "utf8_general_ci"
COLLATE FB__________: false
VIEW CREATION_______: true
STORED PROCEDURE____: true
********************************************************************************

--------------------------------------
DATABASE-ENVIRONMENT
--------------------------------------
MYSQL VERSION:	This Server: 10.0.38 -- Build Server: 10.0.32
FILE SIZE:	dup-database__[HASH].sql (2.47MB)
TIMEOUT:	5000
MAXPACK:	33554432
SQLMODE:	NOT_SET
NEW SQL FILE:	[/home/store.ztech.vn/public_html/dup-installer/dup-installer-data__[HASH].sql]
COLLATE FB:	Off
--------------------------------------
DATABASE RESULTS
--------------------------------------
DB VIEWS:	enabled
DB PROCEDURES:	enabled
ERRORS FOUND:	0
DROPPED TABLES:	40
RENAMED TABLES:	0
QUERIES RAN:	2132

wp_actionscheduler_actions: (1)
wp_actionscheduler_claims: (0)
wp_actionscheduler_groups: (1)
wp_actionscheduler_logs: (1)
wp_commentmeta: (0)
wp_comments: (0)
wp_duplicator_packages: (0)
wp_links: (0)
wp_options: (356)
wp_postmeta: (756)
wp_posts: (402)
wp_social_users: (0)
wp_term_relationships: (62)
wp_term_taxonomy: (35)
wp_termmeta: (2)
wp_terms: (35)
wp_usermeta: (70)
wp_users: (1)
wp_woocommerce_api_keys: (0)
wp_woocommerce_attribute_taxonomies: (0)
wp_woocommerce_downloadable_product_permissions: (0)
wp_woocommerce_log: (0)
wp_woocommerce_order_itemmeta: (0)
wp_woocommerce_order_items: (0)
wp_woocommerce_payment_tokenmeta: (0)
wp_woocommerce_payment_tokens: (0)
wp_woocommerce_sessions: (1)
wp_woocommerce_shipping_zone_locations: (0)
wp_woocommerce_shipping_zone_methods: (0)
wp_woocommerce_shipping_zones: (0)
wp_woocommerce_tax_rate_locations: (0)
wp_woocommerce_tax_rates: (0)
wp_wpmailsmtp_tasks_meta: (0)
wp_yoast_seo_links: (139)
wp_yoast_seo_meta: (210)
Removed '22' cache/transient rows

INSERT DATA RUNTIME: 1.1752 sec.
STEP-2 COMPLETE @ 08:07:15 - RUNTIME: 1.1833 sec.

====================================
SET SEARCH AND REPLACE LIST
====================================


********************************************************************************
DUPLICATOR PRO INSTALL-LOG
STEP-3 START @ 08:07:20
NOTICE: Do NOT post to public sites or forums
********************************************************************************
CHARSET SERVER:	"utf8"
CHARSET CLIENT:	"utf8"
********************************************************************************
OPTIONS:
blogname______________: "Trang web mới"
postguid______________: false
fullsearch____________: false
path_old______________: "/home/athena.giaodienwebmau.com/public_html"
path_new______________: "/home/store.ztech.vn/public_html"
siteurl_______________: "http://store.ztech.vn"
url_old_______________: "http://athena.giaodienwebmau.com"
url_new_______________: "http://store.ztech.vn"
maxSerializeStrlen____: 4000000
replaceMail___________: false
dbcharset_____________: "utf8"
dbcollate_____________: "utf8_general_ci"
wp_mail_______________: ""
wp_nickname___________: ""
wp_first_name_________: ""
wp_last_name__________: ""
ssl_admin_____________: false
cache_wp______________: false
cache_path____________: false
exe_safe_mode_________: false
config_mode___________: "NEW"
********************************************************************************


====================================
RUN SEARCH AND REPLACE
====================================

EVALUATE TABLE: "wp_actionscheduler_actions"______________________[ROWS:     1][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/athena.giaodienwebmau.com/public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  2:"\/home\/athena.giaodienwebmau.com\/public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  3:"%2Fhome%2Fathena.giaodienwebmau.com%2Fpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  4:"\home\athena.giaodienwebmau.com\public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  5:"\\home\\athena.giaodienwebmau.com\\public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  6:"%5Chome%5Cathena.giaodienwebmau.com%5Cpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  7:"//athena.giaodienwebmau.com" =====================> "//store.ztech.vn"
	SEARCH  8:"\/\/athena.giaodienwebmau.com" ===================> "\/\/store.ztech.vn"
	SEARCH  9:"%2F%2Fathena.giaodienwebmau.com" =================> "%2F%2Fstore.ztech.vn"
	SEARCH 10:"https://store.ztech.vn" ==========================> "http://store.ztech.vn"
	SEARCH 11:"https:\/\/store.ztech.vn" ========================> "http:\/\/store.ztech.vn"
	SEARCH 12:"https%3A%2F%2Fstore.ztech.vn" ====================> "http%3A%2F%2Fstore.ztech.vn"

EVALUATE TABLE: "wp_actionscheduler_claims"_______________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_actionscheduler_groups"_______________________[ROWS:     1][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/athena.giaodienwebmau.com/public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  2:"\/home\/athena.giaodienwebmau.com\/public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  3:"%2Fhome%2Fathena.giaodienwebmau.com%2Fpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  4:"\home\athena.giaodienwebmau.com\public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  5:"\\home\\athena.giaodienwebmau.com\\public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  6:"%5Chome%5Cathena.giaodienwebmau.com%5Cpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  7:"//athena.giaodienwebmau.com" =====================> "//store.ztech.vn"
	SEARCH  8:"\/\/athena.giaodienwebmau.com" ===================> "\/\/store.ztech.vn"
	SEARCH  9:"%2F%2Fathena.giaodienwebmau.com" =================> "%2F%2Fstore.ztech.vn"
	SEARCH 10:"https://store.ztech.vn" ==========================> "http://store.ztech.vn"
	SEARCH 11:"https:\/\/store.ztech.vn" ========================> "http:\/\/store.ztech.vn"
	SEARCH 12:"https%3A%2F%2Fstore.ztech.vn" ====================> "http%3A%2F%2Fstore.ztech.vn"

EVALUATE TABLE: "wp_actionscheduler_logs"_________________________[ROWS:     1][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/athena.giaodienwebmau.com/public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  2:"\/home\/athena.giaodienwebmau.com\/public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  3:"%2Fhome%2Fathena.giaodienwebmau.com%2Fpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  4:"\home\athena.giaodienwebmau.com\public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  5:"\\home\\athena.giaodienwebmau.com\\public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  6:"%5Chome%5Cathena.giaodienwebmau.com%5Cpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  7:"//athena.giaodienwebmau.com" =====================> "//store.ztech.vn"
	SEARCH  8:"\/\/athena.giaodienwebmau.com" ===================> "\/\/store.ztech.vn"
	SEARCH  9:"%2F%2Fathena.giaodienwebmau.com" =================> "%2F%2Fstore.ztech.vn"
	SEARCH 10:"https://store.ztech.vn" ==========================> "http://store.ztech.vn"
	SEARCH 11:"https:\/\/store.ztech.vn" ========================> "http:\/\/store.ztech.vn"
	SEARCH 12:"https%3A%2F%2Fstore.ztech.vn" ====================> "http%3A%2F%2Fstore.ztech.vn"

EVALUATE TABLE: "wp_commentmeta"__________________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_comments"_____________________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_duplicator_packages"__________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_links"________________________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_options"______________________________________[ROWS:   356][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/athena.giaodienwebmau.com/public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  2:"\/home\/athena.giaodienwebmau.com\/public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  3:"%2Fhome%2Fathena.giaodienwebmau.com%2Fpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  4:"\home\athena.giaodienwebmau.com\public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  5:"\\home\\athena.giaodienwebmau.com\\public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  6:"%5Chome%5Cathena.giaodienwebmau.com%5Cpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  7:"//athena.giaodienwebmau.com" =====================> "//store.ztech.vn"
	SEARCH  8:"\/\/athena.giaodienwebmau.com" ===================> "\/\/store.ztech.vn"
	SEARCH  9:"%2F%2Fathena.giaodienwebmau.com" =================> "%2F%2Fstore.ztech.vn"
	SEARCH 10:"https://store.ztech.vn" ==========================> "http://store.ztech.vn"
	SEARCH 11:"https:\/\/store.ztech.vn" ========================> "http:\/\/store.ztech.vn"
	SEARCH 12:"https%3A%2F%2Fstore.ztech.vn" ====================> "http%3A%2F%2Fstore.ztech.vn"

EVALUATE TABLE: "wp_postmeta"_____________________________________[ROWS:   756][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/athena.giaodienwebmau.com/public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  2:"\/home\/athena.giaodienwebmau.com\/public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  3:"%2Fhome%2Fathena.giaodienwebmau.com%2Fpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  4:"\home\athena.giaodienwebmau.com\public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  5:"\\home\\athena.giaodienwebmau.com\\public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  6:"%5Chome%5Cathena.giaodienwebmau.com%5Cpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  7:"//athena.giaodienwebmau.com" =====================> "//store.ztech.vn"
	SEARCH  8:"\/\/athena.giaodienwebmau.com" ===================> "\/\/store.ztech.vn"
	SEARCH  9:"%2F%2Fathena.giaodienwebmau.com" =================> "%2F%2Fstore.ztech.vn"
	SEARCH 10:"https://store.ztech.vn" ==========================> "http://store.ztech.vn"
	SEARCH 11:"https:\/\/store.ztech.vn" ========================> "http:\/\/store.ztech.vn"
	SEARCH 12:"https%3A%2F%2Fstore.ztech.vn" ====================> "http%3A%2F%2Fstore.ztech.vn"

EVALUATE TABLE: "wp_posts"________________________________________[ROWS:   402][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/athena.giaodienwebmau.com/public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  2:"\/home\/athena.giaodienwebmau.com\/public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  3:"%2Fhome%2Fathena.giaodienwebmau.com%2Fpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  4:"\home\athena.giaodienwebmau.com\public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  5:"\\home\\athena.giaodienwebmau.com\\public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  6:"%5Chome%5Cathena.giaodienwebmau.com%5Cpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  7:"//athena.giaodienwebmau.com" =====================> "//store.ztech.vn"
	SEARCH  8:"\/\/athena.giaodienwebmau.com" ===================> "\/\/store.ztech.vn"
	SEARCH  9:"%2F%2Fathena.giaodienwebmau.com" =================> "%2F%2Fstore.ztech.vn"
	SEARCH 10:"https://store.ztech.vn" ==========================> "http://store.ztech.vn"
	SEARCH 11:"https:\/\/store.ztech.vn" ========================> "http:\/\/store.ztech.vn"
	SEARCH 12:"https%3A%2F%2Fstore.ztech.vn" ====================> "http%3A%2F%2Fstore.ztech.vn"

EVALUATE TABLE: "wp_social_users"_________________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_term_relationships"___________________________[ROWS:    62][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/athena.giaodienwebmau.com/public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  2:"\/home\/athena.giaodienwebmau.com\/public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  3:"%2Fhome%2Fathena.giaodienwebmau.com%2Fpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  4:"\home\athena.giaodienwebmau.com\public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  5:"\\home\\athena.giaodienwebmau.com\\public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  6:"%5Chome%5Cathena.giaodienwebmau.com%5Cpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  7:"//athena.giaodienwebmau.com" =====================> "//store.ztech.vn"
	SEARCH  8:"\/\/athena.giaodienwebmau.com" ===================> "\/\/store.ztech.vn"
	SEARCH  9:"%2F%2Fathena.giaodienwebmau.com" =================> "%2F%2Fstore.ztech.vn"
	SEARCH 10:"https://store.ztech.vn" ==========================> "http://store.ztech.vn"
	SEARCH 11:"https:\/\/store.ztech.vn" ========================> "http:\/\/store.ztech.vn"
	SEARCH 12:"https%3A%2F%2Fstore.ztech.vn" ====================> "http%3A%2F%2Fstore.ztech.vn"

EVALUATE TABLE: "wp_term_taxonomy"________________________________[ROWS:    35][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/athena.giaodienwebmau.com/public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  2:"\/home\/athena.giaodienwebmau.com\/public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  3:"%2Fhome%2Fathena.giaodienwebmau.com%2Fpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  4:"\home\athena.giaodienwebmau.com\public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  5:"\\home\\athena.giaodienwebmau.com\\public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  6:"%5Chome%5Cathena.giaodienwebmau.com%5Cpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  7:"//athena.giaodienwebmau.com" =====================> "//store.ztech.vn"
	SEARCH  8:"\/\/athena.giaodienwebmau.com" ===================> "\/\/store.ztech.vn"
	SEARCH  9:"%2F%2Fathena.giaodienwebmau.com" =================> "%2F%2Fstore.ztech.vn"
	SEARCH 10:"https://store.ztech.vn" ==========================> "http://store.ztech.vn"
	SEARCH 11:"https:\/\/store.ztech.vn" ========================> "http:\/\/store.ztech.vn"
	SEARCH 12:"https%3A%2F%2Fstore.ztech.vn" ====================> "http%3A%2F%2Fstore.ztech.vn"

EVALUATE TABLE: "wp_termmeta"_____________________________________[ROWS:     2][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/athena.giaodienwebmau.com/public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  2:"\/home\/athena.giaodienwebmau.com\/public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  3:"%2Fhome%2Fathena.giaodienwebmau.com%2Fpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  4:"\home\athena.giaodienwebmau.com\public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  5:"\\home\\athena.giaodienwebmau.com\\public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  6:"%5Chome%5Cathena.giaodienwebmau.com%5Cpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  7:"//athena.giaodienwebmau.com" =====================> "//store.ztech.vn"
	SEARCH  8:"\/\/athena.giaodienwebmau.com" ===================> "\/\/store.ztech.vn"
	SEARCH  9:"%2F%2Fathena.giaodienwebmau.com" =================> "%2F%2Fstore.ztech.vn"
	SEARCH 10:"https://store.ztech.vn" ==========================> "http://store.ztech.vn"
	SEARCH 11:"https:\/\/store.ztech.vn" ========================> "http:\/\/store.ztech.vn"
	SEARCH 12:"https%3A%2F%2Fstore.ztech.vn" ====================> "http%3A%2F%2Fstore.ztech.vn"

EVALUATE TABLE: "wp_terms"________________________________________[ROWS:    35][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/athena.giaodienwebmau.com/public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  2:"\/home\/athena.giaodienwebmau.com\/public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  3:"%2Fhome%2Fathena.giaodienwebmau.com%2Fpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  4:"\home\athena.giaodienwebmau.com\public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  5:"\\home\\athena.giaodienwebmau.com\\public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  6:"%5Chome%5Cathena.giaodienwebmau.com%5Cpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  7:"//athena.giaodienwebmau.com" =====================> "//store.ztech.vn"
	SEARCH  8:"\/\/athena.giaodienwebmau.com" ===================> "\/\/store.ztech.vn"
	SEARCH  9:"%2F%2Fathena.giaodienwebmau.com" =================> "%2F%2Fstore.ztech.vn"
	SEARCH 10:"https://store.ztech.vn" ==========================> "http://store.ztech.vn"
	SEARCH 11:"https:\/\/store.ztech.vn" ========================> "http:\/\/store.ztech.vn"
	SEARCH 12:"https%3A%2F%2Fstore.ztech.vn" ====================> "http%3A%2F%2Fstore.ztech.vn"

EVALUATE TABLE: "wp_usermeta"_____________________________________[ROWS:    70][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/athena.giaodienwebmau.com/public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  2:"\/home\/athena.giaodienwebmau.com\/public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  3:"%2Fhome%2Fathena.giaodienwebmau.com%2Fpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  4:"\home\athena.giaodienwebmau.com\public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  5:"\\home\\athena.giaodienwebmau.com\\public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  6:"%5Chome%5Cathena.giaodienwebmau.com%5Cpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  7:"//athena.giaodienwebmau.com" =====================> "//store.ztech.vn"
	SEARCH  8:"\/\/athena.giaodienwebmau.com" ===================> "\/\/store.ztech.vn"
	SEARCH  9:"%2F%2Fathena.giaodienwebmau.com" =================> "%2F%2Fstore.ztech.vn"
	SEARCH 10:"https://store.ztech.vn" ==========================> "http://store.ztech.vn"
	SEARCH 11:"https:\/\/store.ztech.vn" ========================> "http:\/\/store.ztech.vn"
	SEARCH 12:"https%3A%2F%2Fstore.ztech.vn" ====================> "http%3A%2F%2Fstore.ztech.vn"

EVALUATE TABLE: "wp_users"________________________________________[ROWS:     1][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/athena.giaodienwebmau.com/public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  2:"\/home\/athena.giaodienwebmau.com\/public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  3:"%2Fhome%2Fathena.giaodienwebmau.com%2Fpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  4:"\home\athena.giaodienwebmau.com\public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  5:"\\home\\athena.giaodienwebmau.com\\public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  6:"%5Chome%5Cathena.giaodienwebmau.com%5Cpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  7:"//athena.giaodienwebmau.com" =====================> "//store.ztech.vn"
	SEARCH  8:"\/\/athena.giaodienwebmau.com" ===================> "\/\/store.ztech.vn"
	SEARCH  9:"%2F%2Fathena.giaodienwebmau.com" =================> "%2F%2Fstore.ztech.vn"
	SEARCH 10:"https://store.ztech.vn" ==========================> "http://store.ztech.vn"
	SEARCH 11:"https:\/\/store.ztech.vn" ========================> "http:\/\/store.ztech.vn"
	SEARCH 12:"https%3A%2F%2Fstore.ztech.vn" ====================> "http%3A%2F%2Fstore.ztech.vn"

EVALUATE TABLE: "wp_woocommerce_api_keys"_________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_woocommerce_attribute_taxonomies"_____________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_woocommerce_downloadable_product_permissions"_[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_woocommerce_log"______________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_woocommerce_order_itemmeta"___________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_woocommerce_order_items"______________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_woocommerce_payment_tokenmeta"________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_woocommerce_payment_tokens"___________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_woocommerce_sessions"_________________________[ROWS:     1][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/athena.giaodienwebmau.com/public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  2:"\/home\/athena.giaodienwebmau.com\/public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  3:"%2Fhome%2Fathena.giaodienwebmau.com%2Fpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  4:"\home\athena.giaodienwebmau.com\public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  5:"\\home\\athena.giaodienwebmau.com\\public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  6:"%5Chome%5Cathena.giaodienwebmau.com%5Cpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  7:"//athena.giaodienwebmau.com" =====================> "//store.ztech.vn"
	SEARCH  8:"\/\/athena.giaodienwebmau.com" ===================> "\/\/store.ztech.vn"
	SEARCH  9:"%2F%2Fathena.giaodienwebmau.com" =================> "%2F%2Fstore.ztech.vn"
	SEARCH 10:"https://store.ztech.vn" ==========================> "http://store.ztech.vn"
	SEARCH 11:"https:\/\/store.ztech.vn" ========================> "http:\/\/store.ztech.vn"
	SEARCH 12:"https%3A%2F%2Fstore.ztech.vn" ====================> "http%3A%2F%2Fstore.ztech.vn"

EVALUATE TABLE: "wp_woocommerce_shipping_zone_locations"__________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_woocommerce_shipping_zone_methods"____________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_woocommerce_shipping_zones"___________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_woocommerce_tax_rate_locations"_______________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_woocommerce_tax_rates"________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_wpmailsmtp_tasks_meta"________________________[ROWS:     0][PG:   0][SCAN:no columns  ]

EVALUATE TABLE: "wp_yoast_seo_links"______________________________[ROWS:   139][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/athena.giaodienwebmau.com/public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  2:"\/home\/athena.giaodienwebmau.com\/public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  3:"%2Fhome%2Fathena.giaodienwebmau.com%2Fpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  4:"\home\athena.giaodienwebmau.com\public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  5:"\\home\\athena.giaodienwebmau.com\\public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  6:"%5Chome%5Cathena.giaodienwebmau.com%5Cpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  7:"//athena.giaodienwebmau.com" =====================> "//store.ztech.vn"
	SEARCH  8:"\/\/athena.giaodienwebmau.com" ===================> "\/\/store.ztech.vn"
	SEARCH  9:"%2F%2Fathena.giaodienwebmau.com" =================> "%2F%2Fstore.ztech.vn"
	SEARCH 10:"https://store.ztech.vn" ==========================> "http://store.ztech.vn"
	SEARCH 11:"https:\/\/store.ztech.vn" ========================> "http:\/\/store.ztech.vn"
	SEARCH 12:"https%3A%2F%2Fstore.ztech.vn" ====================> "http%3A%2F%2Fstore.ztech.vn"

EVALUATE TABLE: "wp_yoast_seo_meta"_______________________________[ROWS:   210][PG:   1][SCAN:text columns]
	SEARCH  1:"/home/athena.giaodienwebmau.com/public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  2:"\/home\/athena.giaodienwebmau.com\/public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  3:"%2Fhome%2Fathena.giaodienwebmau.com%2Fpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  4:"\home\athena.giaodienwebmau.com\public_html" =====> "/home/store.ztech.vn/public_html"
	SEARCH  5:"\\home\\athena.giaodienwebmau.com\\public_html" ==> "\/home\/store.ztech.vn\/public_html"
	SEARCH  6:"%5Chome%5Cathena.giaodienwebmau.com%5Cpublic_html" => "%2Fhome%2Fstore.ztech.vn%2Fpublic_html"
	SEARCH  7:"//athena.giaodienwebmau.com" =====================> "//store.ztech.vn"
	SEARCH  8:"\/\/athena.giaodienwebmau.com" ===================> "\/\/store.ztech.vn"
	SEARCH  9:"%2F%2Fathena.giaodienwebmau.com" =================> "%2F%2Fstore.ztech.vn"
	SEARCH 10:"https://store.ztech.vn" ==========================> "http://store.ztech.vn"
	SEARCH 11:"https:\/\/store.ztech.vn" ========================> "http:\/\/store.ztech.vn"
	SEARCH 12:"https%3A%2F%2Fstore.ztech.vn" ====================> "http%3A%2F%2Fstore.ztech.vn"
--------------------------------------
SCANNED:	Tables:35 	|	 Rows:2072 	|	 Cells:15877 
UPDATED:	Tables:5 	|	 Rows:525 	|	 Cells:606 
ERRORS:		0 
RUNTIME:	0.223700 sec

====================================
REMOVE LICENSE KEY
====================================

====================================
CREATE NEW ADMIN USER
====================================

====================================
CONFIGURATION FILE UPDATES
====================================
	UPDATE DB_NAME ""db_storez""
	UPDATE DB_USER ""user_storez""
	UPDATE DB_PASSWORD "** OBSCURED **"
	UPDATE DB_HOST ""localhost""
	REMOVE WPCACHEHOME
	
*** UPDATED WP CONFIG FILE ***

====================================
HTACCESS UPDATE MODE: "NEW"
====================================
- PASS: Successfully created a new .htaccess file.
- PASS: Existing Apache '.htaccess__[HASH]' was removed
- PASS: Existing Microsoft IIS 'web.config.orig' was removed

====================================
GENERAL UPDATES & CLEANUP
====================================

====================================
DEACTIVATE PLUGINS CHECK
====================================
Deactivated plugins list here: Array
(
    [0] => really-simple-ssl/rlrsssl-really-simple-ssl.php
    [1] => simple-google-recaptcha/simple-google-recaptcha.php
    [2] => js_composer/js_composer.php
)


====================================
NOTICES TEST
====================================
No General Notices Found


====================================
CLEANUP TMP FILES
====================================

====================================
FINAL REPORT NOTICES
====================================

STEP-3 COMPLETE @ 08:07:20 - RUNTIME: 0.2460 sec. 


====================================
FINAL REPORT NOTICES LIST
====================================
-----------------------
[INFO] No general notices
	SECTIONS: general

-----------------------
[INFO] No errors in database
	SECTIONS: database

-----------------------
[INFO] No search and replace data errors
	SECTIONS: search_replace

-----------------------
[INFO] No files extraction errors
	SECTIONS: files

====================================
