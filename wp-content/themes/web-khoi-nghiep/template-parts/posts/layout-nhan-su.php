<?php 
	do_action('flatsome_before_blog');
?>

<?php if(!is_single() && flatsome_option('blog_featured') == 'top'){ get_template_part('template-parts/posts/featured-posts'); } ?>
<div class="row row-large <?php if(flatsome_option('blog_layout_divider')) echo 'row-divided ';?>">

	<div class="post-sidebar large-3 col">
 <?php the_post_thumbnail('medium'); ?>
	</div><!-- .post-sidebar -->

	<div class="large-9 col medium-col-first col-nhan-su">
	<?php if(!is_single() && flatsome_option('blog_featured') == 'content'){ get_template_part('template-parts/posts/featured-posts'); } ?>
	<?php
		if(is_single()){
			get_template_part( 'template-parts/posts/single-nhan-su');

		} else{
			get_template_part( 'template-parts/posts/archive', flatsome_option('blog_style') );
		}	?>
	</div> <!-- .large-9 -->
	<div>
		
<?php $orig_post = $post;
global $post;
$categories = get_the_category($post->ID);
if ($categories) {
$category_ids = array();
foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
$args=array(
'category__in' => $category_ids,
'post__not_in' => array($post->ID),
'posts_per_page'=> 3, // Number of related posts that will be shown.
'caller_get_posts'=>1
);
$my_query = new wp_query( $args );
if( $my_query->have_posts() ) {
?>
<div class="related-post">
<div  class="" style="margin-top: 30px;margin-bottom: 20px"> 	<div class="duong-line"></div></div>
<div class="clearfix"></div>
   <div class="row large-columns-3 medium-columns-2 small-columns-1 has-shadow row-box-shadow-1 row-box-shadow-1-hover">
<?php
while( $my_query->have_posts() ) {
$my_query->the_post();?>


 
  		<div class="col post-item">
			<div class="col-inner">
			<a href="<?php the_permalink()?>" class="plain">
				<div class="box box-overlay dark box-text-bottom box-blog-post has-hover">
            					<div class="box-image">
  						<div class="image-cover" >
  							<?php the_post_thumbnail(); ?>						  							  						</div>
  						  					</div><!-- .box-image -->
          					<div class="box-text text-center">
					<div class="box-text-inner blog-post-inner">

					
										<h5 class="post-title is-large "><?php the_title(); ?></h5>
					
										<p><?php $chucvu=get_field('chuc_vu');
					if($chucvu !=""){
						echo $chucvu;
					}
					
					?></p>
										
					
					
					</div><!-- .box-text-inner -->
					</div><!-- .box-text -->
									</div><!-- .box -->
				</a><!-- .link -->
			</div><!-- .col-inner -->
		</div><!-- .col -->
	
	



<?php
}
?>
</div>
 </div>

<?php
}
}
$post = $orig_post;
wp_reset_query(); ?>


	</div>

</div><!-- .row -->

<?php 
	do_action('flatsome_after_blog');
?>