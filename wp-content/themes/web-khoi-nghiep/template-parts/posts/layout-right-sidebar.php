<?php 
	do_action('flatsome_before_blog');
?>
<?php echo do_shortcode('[block id="block-header-post"]' );?>

<?php if(!is_single() && flatsome_option('blog_featured') == 'top'){ get_template_part('template-parts/posts/featured-posts'); } ?>

<div class="row row-large <?php if(flatsome_option('blog_layout_divider')) echo 'row-divided ';?>">
	
	<div class="large-9 col">
	<?php if(!is_single() && flatsome_option('blog_featured') == 'content'){ get_template_part('template-parts/posts/featured-posts'); } ?>
	<?php
		if(is_single()){
			get_template_part( 'template-parts/posts/single');
			comments_template();
		} elseif(flatsome_option('blog_style_archive') && (is_archive() || is_search())){
			get_template_part( 'template-parts/posts/archive', flatsome_option('blog_style_archive') );
		} else {
			get_template_part( 'template-parts/posts/archive', flatsome_option('blog_style') );
		}
	?>
	</div> <!-- .large-9 -->

	<div class="post-sidebar large-3 col">
		<?php get_sidebar(); ?>
	</div><!-- .post-sidebar -->

<?php if ( get_theme_mod( 'blog_single_next_prev_nav', 1 ) ) :
	flatsome_content_nav( 'nav-below' );
endif; ?>



<?php $orig_post = $post;
global $post;
$categories = get_the_category($post->ID);
if ($categories) {
$category_ids = array();
foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
$args=array(
'category__in' => $category_ids,
'post__not_in' => array($post->ID),
'posts_per_page'=> 3, // Number of related posts that will be shown.
'caller_get_posts'=>1
);
$my_query = new wp_query( $args );
if( $my_query->have_posts() ) {
?>
<div class="related-post">
<div  class="tieu-de-lien-quan" style="margin-top: 30px;margin-bottom: 20px"> <h6>Bài viết cùng chủ đề</h6></div>
<div class="clearfix"></div>
   <div class="row large-columns-3 medium-columns-2 small-columns-1 has-shadow row-box-shadow-1 row-box-shadow-1-hover">
<?php
while( $my_query->have_posts() ) {
$my_query->the_post();?>


 
  		<div class="col post-item">
			<div class="col-inner">
			<a href="<?php the_permalink()?>" class="plain">
				<div class="box box-bounce box-text-bottom box-blog-post has-hover">
            					<div class="box-image">
  						<div class="image-cover" style="padding-top:56%;">
  							<?php the_post_thumbnail(); ?>						  							  						</div>
  						  					</div><!-- .box-image -->
          					<div class="box-text text-left">
					<div class="box-text-inner blog-post-inner">
						<?php if($show_category !== 'false') { ?>
						<p class="cat-label <?php if($show_category == 'label') echo 'tag-label'; ?> is-xxsmall op-7 uppercase">
					<?php
						foreach((get_the_category()) as $cat) {
						echo $cat->cat_name . ' ';
					}
					?>
					</p>
					<?php } ?>
					
										<h5 class="post-title is-large "><?php the_title(); ?></h5>
					<div class="post-meta is-small op-8"><?php the_time('M j, Y') ?></div>					<div class="is-divider"></div>
										
										
					
					
					</div><!-- .box-text-inner -->
					</div><!-- .box-text -->
									</div><!-- .box -->
				</a><!-- .link -->
			</div><!-- .col-inner -->
		</div><!-- .col -->
	
	



<?php
}
?>
</div>
 </div>

<?php
}
}
$post = $orig_post;
wp_reset_query(); ?>




</div><!-- .row -->

<?php 
	do_action('flatsome_after_blog');
?>