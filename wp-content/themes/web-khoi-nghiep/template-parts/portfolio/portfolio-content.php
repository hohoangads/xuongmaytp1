<?php while ( have_posts() ) : the_post(); ?>



	<?php 

$images = get_field('bo_suu_tap');
$size = 'full'; // (thumbnail, medium, large, full or custom size)

if( $images ): ?>


<div class="row row-isotope large-columns-3 medium-columns- small-columns-2 row-xsmall du-an-con" >
                   <?php foreach( $images as $image ): ?>
         
                    <div class="col" >
          <div class="col-inner">
          <a href="<?php echo $image['url']; ?>" class="plain lightbox-gallery">
          <div class="portfolio-box box dark">

            <div class="box-image">
            <span class="hover-span">
            	<i class="fa fa-search"></i>
            </span>
            <?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
            </div><!-- box-image -->
      
           </div><!-- .box  -->
           </a>
           </div><!-- .col-inner -->
           </div><!-- .col -->
            <?php endforeach; ?>
          </div>





  
<?php endif; ?>
		<?php if(get_the_content()) {the_content();} else {
		
		}; ?>
<?php endwhile; wp_reset_query(); // end of the loop. ?>
