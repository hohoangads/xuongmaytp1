<?php
	// RELATED PORTFOLIO
if ( get_theme_mod( 'portfolio_related', 1 ) == 0 ) {
  return;
}

$terms   = get_the_terms( get_the_ID(), 'featured_item_category' );
$term_id = $terms ? current( $terms )->term_id : '';
$height  = get_theme_mod( 'portfolio_height' );
$height  = $height ? $height : '';



  ?>
  <div class="row text-por-related"><h3>Dự án khác cùng loại <i class="icon-plus"></i></h3></div>
  <?php
  echo do_shortcode( '<div class="portfolio-related">[ux_portfolio image_height="' . $height . '" class="portfolio-related" exclude="' . get_the_ID() . '" cat="' . $term_id . '"  image_size="medium"  number="3" image_height="65%" type="row"  columns="3" ]</div>' );
 
?>
