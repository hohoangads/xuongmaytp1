<?php
/*
Template name: Page - Full Width - Transparent header - Light text
*/
get_header("404"); ?>

<?php do_action( 'flatsome_before_page' ); ?>

<div id="content" role="main">
			<?php 
	echo do_shortcode( '[block id="' . get_theme_mod( '404_block' ) . '"]' );?>
		
</div>

<?php do_action( 'flatsome_after_page' ); ?>

<?php get_footer(); ?>


