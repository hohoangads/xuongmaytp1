<?php
// Add custom Theme Functions here
//Copy từng phần và bỏ vào file functions.php của theme:


//Tùy chỉnh admin footer
function custom_admin_footer() { 
 echo 'Thiết kế bởi <a href="http://ztech.vn" target="blank">Ztech Solutions</a>';}
 add_filter('admin_footer_text', 'custom_admin_footer');


//Ẩn các panel không cần thiết
 add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');

function my_custom_dashboard_widgets()
{
     global $wp_meta_boxes;

     // Right Now - Comments, Posts, Pages at a glance
     unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);

     // Recent Comments
     unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);

     // Incoming Links
     unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);

     // Plugins - Popular, New and Recently updated WordPress Plugins
     unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);

     // WordPress Development Blog Feed
     unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);

     // Other WordPress News Feed
     unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);

     // Quick Press Form
     unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);

     // Recent Drafts List
     unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
}



//Ẩn Welcome Panel:
add_action( 'load-index.php', 'hide_welcome_panel' );

function hide_welcome_panel() {
    $user_id = get_current_user_id();

    if ( 1 == get_user_meta( $user_id, 'show_welcome_panel', true ) )
        update_user_meta( $user_id, 'show_welcome_panel', 0 );
}


//Thêm Panel welcome mới:





//Xóa logo wordpress
add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );

function remove_wp_logo( $wp_admin_bar ) {
    $wp_admin_bar->remove_node( 'wp-logo' );
}



//Ẩn cập nhật woo

//Remove WooCommerce's annoying update message
remove_action( 'admin_notices', 'woothemes_updater_notice' );

// REMOVE THE WORDPRESS UPDATE NOTIFICATION FOR ALL USERS EXCEPT ADMIN
   global $user_login;
   get_currentuserinfo();
   if (!current_user_can('update_plugins'))
   {
        // checks to see if current user can update plugins
           add_action( 'init', create_function( '$a', "remove_action( 'init', 'wp_version_check' );" ), 2 );
           add_filter( 'pre_option_update_core', create_function( '$a', "return null;" ) );
   }

//xoa mã bưu điện thanh toán
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
function custom_override_checkout_fields( $fields ) {
     unset($fields['billing']['billing_postcode']);
     unset($fields['billing']['billing_country']);
     unset($fields['billing']['billing_address_2']);
     unset($fields['billing']['billing_company']);
     
    
     return $fields;
}
// Custom Dashboard
function my_custom_dashboard() {
    $screen = get_current_screen();
    if( $screen->base == 'dashboard' ) {
        include 'admin/dashboard-panel.php';
    }
}
add_action('admin_notices', 'my_custom_dashboard');





add_filter('woocommerce_empty_price_html', 'custom_call_for_price');

function custom_call_for_price() {
return '<span class="lien-he-price">Liên hệ: 0903.220.457</span>';
}

function register_my_menu() {
  register_nav_menu('product-menu',__( 'Menu Danh mục' ));
}
add_action( 'init', 'register_my_menu' );



//Doan code thay chữ giảm giá bằng % sale

//* Add stock status to archive pages
add_filter( 'woocommerce_get_availability', 'custom_override_get_availability', 1, 2);
 
// The hook in function $availability is passed via the filter!
function custom_override_get_availability( $availability, $_product ) {
if ( $_product->is_in_stock() ) $availability['availability'] = __('Còn hàng', 'woocommerce');
return $availability;
}

// Thay doi duong dan logo admin
function wpc_url_login(){
return "https://ztech.vn"; // duong dan vao website cua ban
}
add_filter('login_headerurl', 'wpc_url_login');
// Thay doi logo admin wordpress
function login_css() {
wp_enqueue_style( 'login_css', get_stylesheet_directory_uri() . '/login1.css' ); // duong dan den file css moi
}
add_action('login_head', 'login_css');
// Enqueue Scripts and Styles.
add_action( 'wp_enqueue_scripts', 'flatsome_enqueue_scripts_styles' );
function flatsome_enqueue_scripts_styles() {
wp_enqueue_style( 'dashicons' );
wp_enqueue_style( 'flatsome-ionicons', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
}


// Dịch woocommerce


function ra_change_translate_text( $translated_text ) {
if ( $translated_text == 'Old Text' ) {
$translated_text = 'New Translation';
}
return $translated_text;
}
add_filter( 'gettext', 'ra_change_translate_text', 20 );
function ra_change_translate_text_multiple( $translated ) {
$text = array(
'Continue Shopping' => 'Tiếp tục mua hàng',
'Update cart' => 'Cập nhật giỏ hàng',
'Apply Coupon' => 'Áp dụng mã ưu đãi',
'WooCommerce' => 'Quản lý bán hàng',











);
$translated = str_ireplace( array_keys($text), $text, $translated );
return $translated;
}
add_filter( 'gettext', 'ra_change_translate_text_multiple', 20 );
// End dich

function ah_remove_custom_post_type_slug( $post_link, $post, $leavename ) {
 
    if ( ! in_array( $post->post_type, array( 'featured_item' ) ) || 'publish' != $post->post_status )
        return $post_link;
 
    $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );
 
    return $post_link;
}
add_filter( 'post_type_link', 'ah_remove_custom_post_type_slug', 10, 3 );
 
function ah_parse_request_tricksy( $query ) {
    
    if ( ! $query->is_main_query() )
        return;
 
    if ( 2 != count( $query->query )
        || ! isset( $query->query['page'] ) )
        return;
 
    if ( ! empty( $query->query['name'] ) )
        $query->set( 'post_type', array( 'post', 'featured_item', 'page' ) );
}
add_action( 'pre_get_posts', 'ah_parse_request_tricksy' );
add_filter('request', 'rudr_change_term_request', 1, 1 );
 
function rudr_change_term_request($query){
 
    $tax_name = 'featured_item_category'; // specify you taxonomy name here, it can be also 'category' or 'post_tag'
 
    // Request for child terms differs, we should make an additional check
    if( $query['attachment'] ) :
        $include_children = true;
        $name = $query['attachment'];
    else:
        $include_children = false;
        $name = $query['name'];
    endif;
 
 
    $term = get_term_by('slug', $name, $tax_name); // get the current term to make sure it exists
 
    if (isset($name) && $term && !is_wp_error($term)): // check it here
 
        if( $include_children ) {
            unset($query['attachment']);
            $parent = $term->parent;
            while( $parent ) {
                $parent_term = get_term( $parent, $tax_name);
                $name = $parent_term->slug . '/' . $name;
                $parent = $parent_term->parent;
            }
        } else {
            unset($query['name']);
        }
 
        switch( $tax_name ):
            case 'category':{
                $query['category_name'] = $name; // for categories
                break;
            }
            case 'post_tag':{
                $query['tag'] = $name; // for post tags
                break;
            }
            default:{
                $query[$tax_name] = $name; // for another taxonomies
                break;
            }
        endswitch;
 
    endif;
 
    return $query;
 
}
 
 
add_filter( 'term_link', 'rudr_term_permalink', 10, 3 );
 
function rudr_term_permalink( $url, $term, $taxonomy ){
 
    $taxonomy_name = 'featured_item_category'; // your taxonomy name here
    $taxonomy_slug = 'featured_item_category'; // the taxonomy slug can be different with the taxonomy name (like 'post_tag' and 'tag' )
 
    // exit the function if taxonomy slug is not in URL
    if ( strpos($url, $taxonomy_slug) === FALSE || $taxonomy != $taxonomy_name ) return $url;
 
    $url = str_replace('/' . $taxonomy_slug, '', $url);
 
    return $url;
}

/*
* Remove product-category in URL
* Thay danh-muc bằng slug hiện tại của bạn. Themes tại WEb Khởi Nghiệp - Mặc định là danh-muc
*/
add_filter( 'term_link', 'devvn_product_cat_permalink', 10, 3 );
function devvn_product_cat_permalink( $url, $term, $taxonomy ){
    switch ($taxonomy):
        case 'product_cat':
            $taxonomy_slug = 'danh-muc'; //Thay bằng slug hiện tại của bạn. Mặc định Của WKN là danh-muc
            if(strpos($url, $taxonomy_slug) === FALSE) break;
            $url = str_replace('/' . $taxonomy_slug, '', $url);
            break;
    endswitch;
    return $url;
}
// Add our custom product cat rewrite rules
function devvn_product_category_rewrite_rules($flash = false) {
    $terms = get_terms( array(
        'taxonomy' => 'product_cat',
        'post_type' => 'product',
        'hide_empty' => false,
    ));
    if($terms && !is_wp_error($terms)){
        $siteurl = esc_url(home_url('/'));
        foreach ($terms as $term){
            $term_slug = $term->slug;
            $baseterm = str_replace($siteurl,'',get_term_link($term->term_id,'product_cat'));
            add_rewrite_rule($baseterm.'?$','index.php?product_cat='.$term_slug,'top');
            add_rewrite_rule($baseterm.'page/([0-9]{1,})/?$', 'index.php?product_cat='.$term_slug.'&paged=$matches[1]','top');
            add_rewrite_rule($baseterm.'(?:feed/)?(feed|rdf|rss|rss2|atom)/?$', 'index.php?product_cat='.$term_slug.'&feed=$matches[1]','top');
        }
    }
    if ($flash == true)
        flush_rewrite_rules(false);
}
add_action('init', 'devvn_product_category_rewrite_rules');
/*Sửa lỗi khi tạo mới taxomony bị 404*/
add_action( 'create_term', 'devvn_new_product_cat_edit_success', 10, 2 );
function devvn_new_product_cat_edit_success( $term_id, $taxonomy ) {
    devvn_product_category_rewrite_rules(true);
}

/*
* Code Bỏ /san-pham/ hoặc ... có hỗ trợ dạng %product_cat%
*/
function devvn_remove_slug( $post_link, $post ) {
    if ( !in_array( get_post_type($post), array( 'product' ) ) || 'publish' != $post->post_status ) {
        return $post_link;
    }
    if('product' == $post->post_type){
        $post_link = str_replace( '/san-pham/', '/', $post_link ); //Thay cua-hang bằng slug hiện tại của bạn
    }else{
        $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );
    }
    return $post_link;
}
add_filter( 'post_type_link', 'devvn_remove_slug', 10, 2 );
/*Sửa lỗi 404 sau khi đã remove slug product hoặc cua-hang*/
function devvn_woo_product_rewrite_rules($flash = false) {
    global $wp_post_types, $wpdb;
    $siteLink = esc_url(home_url('/'));
    foreach ($wp_post_types as $type=>$custom_post) {
        if($type == 'product'){
            if ($custom_post->_builtin == false) {
                $querystr = "SELECT {$wpdb->posts}.post_name, {$wpdb->posts}.ID
                            FROM {$wpdb->posts} 
                            WHERE {$wpdb->posts}.post_status = 'publish' 
                            AND {$wpdb->posts}.post_type = '{$type}'";
                $posts = $wpdb->get_results($querystr, OBJECT);
                foreach ($posts as $post) {
                    $current_slug = get_permalink($post->ID);
                    $base_product = str_replace($siteLink,'',$current_slug);
                    add_rewrite_rule($base_product.'?$', "index.php?{$custom_post->query_var}={$post->post_name}", 'top');
                }
            }
        }
    }
    if ($flash == true)
        flush_rewrite_rules(false);
}
add_action('init', 'devvn_woo_product_rewrite_rules');
/*Fix lỗi khi tạo sản phẩm mới bị 404*/
function devvn_woo_new_product_post_save($post_id){
    global $wp_post_types;
    $post_type = get_post_type($post_id);
    foreach ($wp_post_types as $type=>$custom_post) {
        if ($custom_post->_builtin == false && $type == $post_type) {
            devvn_woo_product_rewrite_rules(true);
        }
    }
}
add_action('wp_insert_post', 'devvn_woo_new_product_post_save');